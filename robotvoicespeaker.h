#ifndef ROBOTVOICESPEAKER_H
#define ROBOTVOICESPEAKER_H

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include <Core/System/Network/FlowNetwork/skflowsat.h>
#include <Multimedia/Audio/Voice/sktexttospeech.h>
#include <Core/System/Network/FlowNetwork/skflowgenericsubscriber.h>
#include <Core/System/Network/FlowNetwork/skflowaudiopublisher.h>

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

struct PhonemeLabialValues
{
    bool isInterruption;
    SkString phonemeFile;
    SkArgsMap paramsVal;
    ULong startMS;
    ULong durationMS;

    PhonemeLabialValues()
    {
        startMS = 0;
        durationMS = 0;
    }
};

class RobotVoiceSpeaker extends SkFlowSat
{
    SkString srcName;
    SkTextToSpeech *speaker;

    SkString language;
    uint8_t gender;
    uint8_t age;
    int wordsForMinutes;
    int volume;
    int pitch;
    int range;
    int capitals;
    int wordsPauses;

    SkString labialsDir;

    SkQueue<PhonemeLabialValues *> currentValidLabials;
    SkElapsedTimeMillis labialsChrono;

    SkFlowGenericSubscriber *subscriber;

    SkFlowGenericPublisher *activityPublisher;
    SkFlowGenericPublisher *labialsPublisher;
    SkFlowAudioPublisher *audioPublisher;

    public:
        Constructor(RobotVoiceSpeaker, SkFlowSat);

        Slot(onSpeechStarted);
        Slot(onSpeechFinished);

    private:
        bool onSetup()                                              override;
        void onInit()                                               override;
        void onQuit()                                               override;

        void onFastTick()                                           override;
        void onFlowDataCome(SkFlowChannelData &chData)              override;

        bool hasTargets();

        void checkLabialsProducion();
        void sendCurrentLabial();

        void makeVoiceTest();
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif // ROBOTVOICESPEAKER_H
