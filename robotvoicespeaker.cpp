#include "robotvoicespeaker.h"
#include <Core/System/Filesystem/skfsutils.h>

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(RobotVoiceSpeaker, SkFlowSat)
{
    speaker = nullptr;
    subscriber = nullptr;
    activityPublisher = nullptr;
    labialsPublisher = nullptr;
    audioPublisher = nullptr;

    wordsForMinutes = 0;

    volume = 0;
    pitch = 0;
    range = 0;
    capitals = 0;
    wordsPauses = 0;

    setObjectName("RobotVoiceSpeaker");

    SlotSet(onSpeechStarted);
    SlotSet(onSpeechFinished);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool RobotVoiceSpeaker::onSetup()
{
    SkCli *cli = skApp->appCli();

    AssertKiller(!cli->isUsed("--src-channel"));
    srcName = cli->value("--src-channel").toString();

    language = cli->value("--language").toString();
    gender = cli->value("--gender").toUInt8();
    age = cli->value("--age").toUInt8();
    wordsForMinutes = cli->value("--words-for-minutes").toInt();
    wordsPauses = cli->value("--words-pause").toInt();
    volume = cli->value("--volume").toInt();
    pitch = cli->value("--pitch").toInt();
    range = cli->value("--pitch-range").toInt();
    capitals = cli->value("--capitals").toInt();

    labialsDir = cli->value("--labials-directory").toString();
    labialsDir = SkFsUtils::adjustPathEndSeparator(labialsDir.c_str());

    speaker = new SkTextToSpeech(this);
    speaker->setObjectName(this, "Speaker");

    AssertKiller(!speaker->enable());

    speaker->setVoice(language.c_str(), gender, age);
    speaker->setWordsForMinutes(wordsForMinutes);
    speaker->setVolume(volume);
    speaker->setPitch(pitch);
    speaker->setRange(range);
    speaker->setCapitals(capitals);
    speaker->setWordsPauses(wordsPauses);
    speaker->setPunctuations(0);

    Attach(speaker, speechStarted, this, onSpeechStarted, SkDirect);
    Attach(speaker, speechFinished, this, onSpeechFinished, SkDirect);

    subscriber = new SkFlowGenericSubscriber(this);
    subscriber->setObjectName(this, "Subscriber");

    setupSubscriber(subscriber);

    activityPublisher = new SkFlowGenericPublisher(this);
    activityPublisher->setObjectName(this, "ActivityPublisher");
    activityPublisher->setup("Activity", FT_CTRL_SWITCH, T_UINT8, "", nullptr);

    setupPublisher(activityPublisher);

    labialsPublisher = new SkFlowGenericPublisher(this);
    labialsPublisher->setObjectName(this, "LabialsPublisher");
    labialsPublisher->setup("Labials", FT_BLOB, T_STRING, "text/json", nullptr);

    setupPublisher(labialsPublisher);

    SkAudioParameters p(true, 1, speaker->sampleRate(), 1024, AFMT_SHORT);

    audioPublisher =  new SkFlowAudioPublisher(this);
    audioPublisher->setObjectName(this, "AudioPublisher");

    if (!audioPublisher->setup(p))
        return false;

    audioPublisher->setInputBuffer(&speaker->pcmData());
    audioPublisher->enableFftGaussianFilter(true);

    setupPublisher(audioPublisher);

    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void RobotVoiceSpeaker::onInit()
{
    subscriber->subscribe(srcName.c_str());

    activityPublisher->start();
    labialsPublisher->start();
    audioPublisher->start();
}

void RobotVoiceSpeaker::onQuit()
{
    speaker->disable();
    subscriber->close();

    activityPublisher->stop();
    labialsPublisher->stop();
    audioPublisher->stop();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool RobotVoiceSpeaker::hasTargets()
{
    return activityPublisher->hasTargets() || labialsPublisher->hasTargets() || audioPublisher->hasTargets();
}

void RobotVoiceSpeaker::onFastTick()
{
    if (!speaker->pcmData().isEmpty())
        audioPublisher->tick();

    if (!currentValidLabials.isEmpty() && labialsChrono.stop() > currentValidLabials.first()->startMS)
       sendCurrentLabial();
}

void RobotVoiceSpeaker::onFlowDataCome(SkFlowChannelData &chData)
{
    if (!hasTargets())
        return;

    if (chData.chanID == subscriber->source()->chanID)
    {
        SkString txt = subscriber->lastData().toString();
        speaker->synth(txt.c_str());
    }
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(RobotVoiceSpeaker, onSpeechStarted)
{
    SilentSlotArgsWarning();

    uint8_t activity = 1;
    activityPublisher->tick(&activity, 1);
}

SlotImpl(RobotVoiceSpeaker, onSpeechFinished)
{
    SilentSlotArgsWarning();

    checkLabialsProducion();

    if (!currentValidLabials.isEmpty())
    {
        sendCurrentLabial();
        labialsChrono.start();
    }

    uint8_t activity = 0;
    activityPublisher->tick(&activity, 1);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void RobotVoiceSpeaker::makeVoiceTest()
{
    CStr *test = "Non posso ascoltarti! Questo è solo un test di funzionamento " \
        "per lo speaker. La prova di testo, permette di implementare " \
        "la sillabazione, oltre alla gestione dei fonemi per il movimento " \
        "labiale del mio modello tridimensionale ed i parametri di default per la voce.";

    speaker->synth(test);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void RobotVoiceSpeaker::checkLabialsProducion()
{
    SkQueue<SkTextToSpeechPhoneme *> &phonemesQueue = speaker->phonemes();
    ObjectWarning("CHECKING phonemes production: " << phonemesQueue.count());

    while(!phonemesQueue.isEmpty())
    {
        SkTextToSpeechPhoneme *ph = phonemesQueue.dequeue();

        if (ph->phoneme.startsWith("("))
        {
            PhonemeLabialValues *labial = new PhonemeLabialValues;
            labial->isInterruption = true;
            labial->startMS = ph->timePos;
            labial->durationMS = ph->duration;

            currentValidLabials.enqueue(labial);

            delete ph;
            continue;
        }

        SkString labialFilePath(labialsDir);
        labialFilePath.append("ph-");
        labialFilePath.append(ph->phoneme);
        labialFilePath.append(".json");

        if (SkFsUtils::exists(labialFilePath.c_str()))
        {
            SkArgsMap m;
            SkFsUtils::readJSON(labialFilePath.c_str(), m);

            if (m["valid"].toBool())
            {
                PhonemeLabialValues *labial = new PhonemeLabialValues;
                labial->isInterruption = false;
                labial->startMS = ph->timePos;
                labial->durationMS = ph->duration;
                labial->paramsVal = m;

                currentValidLabials.enqueue(labial);
            }
        }

        else
        {
            SkArgsMap m;
            m["phoneme"] = ph->phoneme;
            m["valid"] = false;

            SkFsUtils::writeJSON(labialFilePath.c_str(), m, true);
            ObjectWarning("WROTE new labial config for phoneme: " << ph->phoneme);
        }

        delete ph;
    }
}

void RobotVoiceSpeaker::sendCurrentLabial()
{
    PhonemeLabialValues *labial = currentValidLabials.dequeue();

    if (labial->isInterruption)
        labialsPublisher->tick("", 1);

    else
    {
        SkString json;
        labial->paramsVal.toString(json);
        labialsPublisher->tick(json.c_str(), json.size());
    }

    delete labial;
}
