TARGET = RobotVoiceSpeaker.bin
TEMPLATE = app

INCLUDEPATH += ../SpecialK/LibSkCore
include(../SpecialK/LibSkCore/LibSkCore.pri)
include(../SpecialK/LibSkCore/module-src.pri)

INCLUDEPATH += ../SpecialK/LibSkAudio
include(../SpecialK/LibSkAudio/LibSkAudio.pri)
include(../SpecialK/LibSkAudio/module-src.pri)

DEFINES += ENABLE_XML
DEFINES += ENABLE_HTTP
DEFINES += ENABLE_SSL

SOURCES += \
    main.cpp \
    robotvoicespeaker.cpp

HEADERS += \
    robotvoicespeaker.h

DISTFILES += \
    skmake-ubuntu.json \
    skmake-void.json
