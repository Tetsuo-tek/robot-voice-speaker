#include "robotvoicespeaker.h"

SkApp *skApp = nullptr;

int main(int argc, char *argv[])
{
    skApp = new SkApp(argc, argv);

    SkCli *cli = skApp->appCli();

    cli->add("--src-channel",       "-s", "",           "Setup the source channel");
    cli->add("--labials-directory", "-d", "labials.d",  "Setup the labials json files directory");
    cli->add("--language",          "-l", "it",         "Language identifier (default is 'it' for Italian)");
    cli->add("--gender",            "-g", "2",          "Voice gender, where 0=none, 1=male, 2=female (default is male)");
    cli->add("--age",               "-a", "25",         "Voice age, where o=none (default us 20");
    cli->add("--words-for-minutes", "-w", "140",        "Speaking speed in word per minute, in [80, 450] (default is 170)");
    cli->add("--words-pause",       "-W", "1",          "Pause between words, with units of 10 ms (at the default speed)");
    cli->add("--volume",            "-v", "100",        "Voice volume, in [0, 100] (default is 100)");
    cli->add("--pitch",             "-p", "60",         "Voice pitch, in [0, 100] (default is 30)");
    cli->add("--pitch-range",       "-P", "40",         "Voice pitch-range, in [0, 100] (default is 50)");
    cli->add("--capitals",          "-c", "3",          "Announce capital letters by 0=none, 1=sound, 2=spelling, and >=3 by raising the pitch (default is 3)");


#if defined(ENABLE_HTTP)
    SkFlowSat::addHttpCLI();
#endif

    skApp->init(5000, 150000, SK_TIMEDLOOP_RT);
    new RobotVoiceSpeaker;

    return skApp->exec();
}
